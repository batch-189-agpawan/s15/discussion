// For outputting data or text into the browser console

console.log('Hello World!')

// [COMMENTS]
// This is a single-line comment (ctrl+/)
/* This is for Multi-line (ctrl+shift+/) for longer descriptions and multi-line paragraphs*/

// SYNTAX AND STATEMENTS
// syntax is the code that makes up a statement:
// console
// log()

// statements are made up of syntax that will be run by the browser:
// console.log()

// [VARIABLES]
// let variables are variables that can be re-asigned
let firstName= 'Enzo';
console.log(firstName);

let lastName='Agpawan';
console.log(lastName);

// Re-assigning a valute to a let variable shows no errors
firstName = 'Elon';
console.log(firstName)

// const variables are variables that CANNOT be re-asigned

const colorOfTheSun = 'yellow';
console.log('The color of the sun is ' + colorOfTheSun)

// Error happens when you try to re-assign value of a const variable
// colorOfTheSun = 'red';
// console.log(colorOfTheSun)

// When declaring a variable, you use a keyword like 'let'
let variable = 'value';

// When re-assigning a value to a variable, you just need the variable name
variableName ='New Value'


// DATA TYPES
// 1. String - Denoted by single OR double quotation marks
let personName ='Enzo Agpawan'

// 2. Number - No quotation marks and numerical value
let personAge= 15

// 3. Boolean - Only 'true' or 'false'
let hasGirlfriend = true

// 4. Array - Denoted by brackets and can contain multiple values inside
let hobbies= ['cycling', 'Reading', 'Coding']

// 5. Object - Denoted by curly braces, and has value name/label for each value.
let person = {
	personName: 'Enzo Agpawan',
	personAge: 15,
	hasGirlfriend: true,
	hobbies: ['cycling', 'Reading', 'Coding']
}

// 6. null - A placeholder for future variable re-assignments
let wallet = null


// console.log each of the variables
console.log(personName)
console.log(personAge)
console.log(hasGirlfriend)
console.log(hobbies)
console.log(person)
console.log(wallet)

// to display single value of an object
console.log(person.personAge)

// to display single value of an array
console.log(hobbies[1])